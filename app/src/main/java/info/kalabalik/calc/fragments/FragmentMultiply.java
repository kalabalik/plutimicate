package info.kalabalik.calc.fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import info.kalabalik.calc.R;

public class FragmentMultiply extends FragmentAll implements View.OnClickListener, TextView.OnEditorActionListener {
    View view;
    private int a;
    private int b;

    public FragmentMultiply() {
    }

    public static FragmentMultiply newInstance() {
        return new FragmentMultiply();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_subtract_multiply_divide, container, false);
        ((TextView) view.findViewById(R.id.instructions)).setText(activity.getResources().getString(R.string.instructionsMultiplication));
        ((EditText) view.findViewById(R.id.answer)).setOnEditorActionListener(this);
        view.findViewById(R.id.submit).setOnClickListener(this);
        final TextView dateTV = view.findViewById(R.id.date);
        dateTV.setText(new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY).format(Calendar.getInstance().getTime()));
        dateTV.setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        poseQuestion();
    }

    public void poseQuestion() {
        if (!ai.getListOfWrongPairs().isEmpty() && Math.random() > ai.getLikelyhood4History()) {
            int index = (int) (Math.random() * ai.getListOfWrongPairs().size());
            a = ai.getListOfWrongPairs().get(index).getA();
            b = ai.getListOfWrongPairs().get(index).getB();
        } else {
            a = (int) (Math.random() * 19 + 2);
            b = (int) (Math.random() * 9 + 2);
        }
        if (ai.pairIsEarlyRepetition(a, b)) {
            poseQuestion();
        } else {
            String question = String.valueOf(a) + " * " + String.valueOf(b);
            ((TextView) view.findViewById(R.id.question)).setText(question);
            ((EditText) view.findViewById(R.id.answer)).setText("");
            view.findViewById(R.id.answer).requestFocus();
        }
        if (ai.getRight() >= 20) {
            view.findViewById(R.id.date).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((actionId != 0 && actionId == EditorInfo.IME_ACTION_DONE) ||
                (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            view.findViewById(R.id.submit).performClick();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        int result;
        try {
            result = Integer.valueOf(((EditText) view.findViewById(R.id.answer)).getText().toString());
        } catch (NumberFormatException nFE) {
            return;
        }
        String evaluation;
        if (result == a * b) {
            evaluation = "Richtig!";
            ai.decrementWrongPairs(a, b);
        } else {
            evaluation = "Leider falsch!";
            ai.incrementWrongPairs(a, b);
        }
        Toast.makeText(activity, evaluation, Toast.LENGTH_SHORT).show();
        ((TextView) view.findViewById(R.id.balance)).setText(getBalanceString());
        poseQuestion();
    }
}