package info.kalabalik.calc.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import info.kalabalik.calc.R;

public class FragmentExpand extends FragmentAll implements View.OnClickListener, TextView.OnEditorActionListener {
    View view;
    private int a;
    private int b;
    private int c;

    private int resultA;
    private int resultB;

    public FragmentExpand() {
    }

    public static FragmentExpand newInstance() {
        return new FragmentExpand();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_expand, container, false);
        ((TextView) view.findViewById(R.id.instructions)).setText(activity.getResources().getString(R.string.instructionsExpansion));
        ((EditText) view.findViewById(R.id.enumeratoranswer)).setOnEditorActionListener(this);
        view.findViewById(R.id.submita).setOnClickListener(this);
        ((EditText) view.findViewById(R.id.denominatoranswer)).setOnEditorActionListener(this);
        view.findViewById(R.id.submitb).setOnClickListener(this);
        final TextView dateTV = view.findViewById(R.id.date);
        dateTV.setText(new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY).format(Calendar.getInstance().getTime()));
        dateTV.setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        poseQuestion();
    }

    public void poseQuestion() {
        if (!ai.getListOfWrongTriples().isEmpty() && Math.random() > ai.getLikelyhood4History()) {
            int index = (int) (Math.random() * ai.getListOfWrongTriples().size());
            a = ai.getListOfWrongTriples().get(index).getA();
            b = ai.getListOfWrongTriples().get(index).getB();
            c = ai.getListOfWrongTriples().get(index).getC();
        } else {
            a = (int) (Math.random() * 19 + 1);
            b = (int) (Math.random() * (19 - a) + a);
            c = (int) (Math.random() * 9 + 2);
        }
        if (ai.tripleIsEarlyRepetition(a, b, c)) {
            poseQuestion();
        } else {
            String enumerator = String.valueOf(a);
            String denominator = String.valueOf(b);
            ((TextView) view.findViewById(R.id.enumeratorquestion)).setText(enumerator);
            ((TextView) view.findViewById(R.id.denominatorquestion)).setText(denominator);
            ((TextView) view.findViewById(R.id.expand)).setText(R.string.expand);
            String expandor = activity.getResources().getString(R.string.expandor) + " " + c;
            ((TextView) view.findViewById(R.id.expandor)).setText(expandor);
            ((EditText) view.findViewById(R.id.enumeratoranswer)).setText("");
            ((EditText) view.findViewById(R.id.denominatoranswer)).setText("");
            view.findViewById(R.id.enumeratoranswer).requestFocus();
        }
        if (ai.getRight() >= 20) {
            view.findViewById(R.id.date).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((actionId != 0 && actionId == EditorInfo.IME_ACTION_DONE) ||
                (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            if (v.getId() == R.id.enumeratoranswer) {
                view.findViewById(R.id.submita).performClick();
                return true;
            }
            if (v.getId() == R.id.denominatoranswer) {
                view.findViewById(R.id.submitb).performClick();
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        try {
            resultA = Integer.valueOf(((EditText) view.findViewById(R.id.enumeratoranswer)).getText().toString());
            resultB = Integer.valueOf(((EditText) view.findViewById(R.id.denominatoranswer)).getText().toString());
        } catch (
                NumberFormatException nFE) {
            return;
        }
        if (resultA == 0 || resultB == 0) {
            return;
        }
        String evaluation;
        if (resultA == a * c && resultB == b * c) {
            evaluation = "Richtig!";
            ai.decrementWrongTriples(a, b, c);
        } else {
            evaluation = "Leider falsch!";
            ai.incrementWrongTriples(a, b, c);
        }
        Toast.makeText(activity, evaluation, Toast.LENGTH_SHORT).show();
        ((TextView) view.findViewById(R.id.balance)).setText(getBalanceString());
        poseQuestion();
    }

    private int ggT(int a, int b) {
        if (a == b) {
            return a;
        } else {
            if (a > b) {
                return (ggT(a - b, b));
            } else {
                return (ggT(b - a, a));
            }
        }
    }
}