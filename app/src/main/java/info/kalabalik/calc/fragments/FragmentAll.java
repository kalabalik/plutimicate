package info.kalabalik.calc.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import info.kalabalik.calc.AI;
import info.kalabalik.calc.R;

public class FragmentAll extends Fragment {
    protected Activity activity;
    protected AI ai;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        ai = new AI();
    }

    protected SpannableStringBuilder getBalanceString() {
        Resources res = this.getResources();
        String rightString =
                "\n\n" + res.getString(R.string.right) + ": " + String.valueOf(ai.getRight()) + "        ";
        String wrongString = res.getString(R.string.wrong) + ": " + String.valueOf(ai.getWrong());
        String balance = rightString + wrongString;

        SpannableStringBuilder ssb = new SpannableStringBuilder(balance);
        ssb.setSpan(new ForegroundColorSpan(Color.GREEN), 0, rightString.length() - 1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.RED), rightString.length(), balance.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ssb;
    }
}
