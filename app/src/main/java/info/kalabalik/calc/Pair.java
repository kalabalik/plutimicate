package info.kalabalik.calc;

public class Pair {
    protected final int a;
    protected final int b;

    Pair(int a, int b){
        this.a = a;
        this.b = b;
    }

    public boolean equals(Pair pair) {
        return this.a == pair.a && this.b == pair.b;
    }

    public int getA(){
        return a;
    }

    public int getB(){
        return b;
    }
}
