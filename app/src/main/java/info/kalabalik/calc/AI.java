package info.kalabalik.calc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class AI {
    private final int[][] matrixOfWrongPairs = new int[21][21];
    private final int[][][] matrixOfWrongTriples = new int[21][21][21];
    private final List<Pair> listOfWrongPairs = new ArrayList<>();
    private final List<Triple> listOfWrongTriples = new ArrayList<>();

    private final Queue<Pair> pairs = new LinkedList<>();
    private final Queue<Triple> triples = new LinkedList<>();

    private int right;
    private int wrong;

    private double likelyhood4History = .5;

    public AI() {
        final int historySize = 5;
        for (int a = 0; a <= 20; a++) {
            for (int b = 0; b <= 20; b++) {
                matrixOfWrongPairs[a][b] = 0;
                for (int c = 0; c <= 20; c++) {
                    matrixOfWrongTriples[a][b][c] = 0;
                }
            }
        }
        for (int i = 0; i < historySize; i++) {
            pairs.add(new Pair(0, 0));
            triples.add(new Triple(0, 0, 0));
        }
    }

    public void incrementWrongPairs(int a, int b) {
        wrong++;
        if (matrixOfWrongPairs[a][b] == 0) {
            listOfWrongPairs.add(new Pair(a, b));
        }
        matrixOfWrongPairs[a][b]++;
    }

    public void incrementWrongTriples(int a, int b, int c) {
        wrong++;
        if (matrixOfWrongTriples[a][b][c] == 0) {
            listOfWrongTriples.add(new Triple(a, b, c));
        }
        matrixOfWrongTriples[a][b][c]++;
    }

    public void decrementWrongPairs(int a, int b) {
        right++;
        if (matrixOfWrongPairs[a][b] == 1) {
            Iterator<Pair> it = listOfWrongPairs.iterator();
            while (it.hasNext()) {
                Pair pair = it.next();
                if (pair.getA() == a && pair.getB() == b) {
                    it.remove();
                }
            }
        }
        if (matrixOfWrongPairs[a][b] > 0) {
            matrixOfWrongPairs[a][b]--;
        }
    }

    public void decrementWrongTriples(int a, int b, int c) {
        right++;
        if (matrixOfWrongTriples[a][b][c] == 1) {
            Iterator<Triple> it = listOfWrongTriples.iterator();
            while (it.hasNext()) {
                Triple triple = it.next();
                if (triple.getA() == a && triple.getB() == b && triple.getC() == c) {
                    it.remove();
                }
            }
        }
        if (matrixOfWrongTriples[a][b][c] > 0) {
            matrixOfWrongTriples[a][b][c]--;
        }
    }

    public boolean pairIsEarlyRepetition(int a, int b) {
        for (Pair pair : pairs) {
            if (pair.getA() == a && pair.getB() == b) {
                return true;
            }
        }
        pairs.remove();
        pairs.add(new Pair(a, b));
        return false;
    }

    public boolean tripleIsEarlyRepetition(int a, int b, int c) {
        for (Triple triple : triples) {
            if (triple.getA() == a && triple.getB() == b && triple.getC() == c) {
                return true;
            }
        }
        triples.remove();
        triples.add(new Triple(a, b, c));
        return false;
    }

    public List<Pair> getListOfWrongPairs() {
        return listOfWrongPairs;
    }

    public List<Triple> getListOfWrongTriples() {
        return listOfWrongTriples;
    }

    public int getRight() {
        return right;
    }

    public int getWrong() {
        return wrong;
    }

    public double getLikelyhood4History() {
        return likelyhood4History;
    }
}