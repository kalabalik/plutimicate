package info.kalabalik.calc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;

import info.kalabalik.calc.fragments.FragmentAdd;
import info.kalabalik.calc.fragments.FragmentDivide;
import info.kalabalik.calc.fragments.FragmentExpand;
import info.kalabalik.calc.fragments.FragmentMultiply;
import info.kalabalik.calc.fragments.FragmentReduce;
import info.kalabalik.calc.fragments.FragmentSubtract;

public class MainActivity extends FragmentActivity {
    private MenuItem addition;
    private MenuItem subtraction;
    private MenuItem multiplication;
    private MenuItem division;
    private MenuItem reduction;
    private MenuItem expansion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        addition = menu.findItem(R.id.addition);
        subtraction = menu.findItem(R.id.subtraction);
        multiplication = menu.findItem(R.id.multiplication);
        division = menu.findItem(R.id.division);
        reduction = menu.findItem(R.id.reduction);
        expansion = menu.findItem(R.id.expansion);
        menu.findItem(R.id.reset);

        onOptionsItemSelected(addition);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        addition.setChecked(item == addition);
        subtraction.setChecked(item == subtraction);
        multiplication.setChecked(item == multiplication);
        division.setChecked(item == division);
        reduction.setChecked(item == reduction);
        expansion.setChecked(item == expansion);

        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.addition:
                fragment = FragmentAdd.newInstance();
                break;
            case R.id.subtraction:
                fragment = FragmentSubtract.newInstance();
                break;
            case R.id.multiplication:
                fragment = FragmentMultiply.newInstance();
                break;
            case R.id.division:
                fragment = FragmentDivide.newInstance();
                break;
            case R.id.reduction:
                fragment = FragmentReduce.newInstance();
                break;
            case R.id.expansion:
                fragment = FragmentExpand.newInstance();
                break;
            case R.id.reset:
                onOptionsItemSelected(addition);
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
        replaceFragment(fragment);
        return true;
    }
}