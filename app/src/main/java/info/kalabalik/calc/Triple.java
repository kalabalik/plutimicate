package info.kalabalik.calc;

public class Triple extends Pair {
    private final int c;

    Triple(int a, int b, int c) {
        super(a, b);
        this.c = c;
    }

    public boolean equals(Triple triple) {
        return this.a == triple.a && this.b == triple.b && this.c == triple.c;
    }

    public int getC() {
        return c;
    }
}
